// Функція для додавання обробників подій до checkbox
function addCheckboxEventListeners() {
  let checkboxes = document.querySelectorAll(".checkbox");

  checkboxes.forEach(function (checkbox) {
    checkbox.addEventListener("click", function () {
      if (checkbox.classList.contains("checked")) {
        checkbox.classList.remove("checked");
      } else {
        checkbox.classList.add("checked");
      }
    });
  });
}

document.addEventListener("DOMContentLoaded", addCheckboxEventListeners);
document.addEventListener("DOMContentLoaded", addDeleteEventListeners);

// Отримуємо необхідні елементи DOM
var addRowIcon = document.getElementById("add-row-icon");
var addRecordForm = document.getElementById("add-record-form");
let addRowButton = document.getElementById("add-row-button");
var modal = document.getElementById("add-row-modal");
var closeModal = document.getElementsByClassName("close")[0];
var cancelButton = document.getElementById("cancel-button");

addRowButton.onclick = function () {
  console.log(1);
  modal.style.display = "flex";
};

// При кліку на кнопку закриття, ховаємо вікно
closeModal.onclick = function () {
  document.getElementById("group").value = "";
  document.getElementById("first-name").value = "";
  document.getElementById("last-name").value = "";
  document.getElementById("gender").value = "";
  document.getElementById("birthday").value = "";
  modal.style.display = "none";
};

cancelButton.onclick = function () {
  document.getElementById("group").value = "";
  document.getElementById("first-name").value = "";
  document.getElementById("last-name").value = "";
  document.getElementById("gender").value = "";
  document.getElementById("birthday").value = "";
};

// Отримуємо посилання на форму та таблицю
var form = document.getElementById("add-record-form");
var table = document.getElementById("data-table");

// Додаємо обробник подій для форми
form.addEventListener("submit", function (event) {
  event.preventDefault(); // Відключаємо стандартну відправку форми

  // Отримуємо значення з полів форми
  var group = document.getElementById("group").value;
  var firstName = document.getElementById("first-name").value;
  var lastName = document.getElementById("last-name").value;
  var gender = document.getElementById("gender").value;
  var birthday = document.getElementById("birthday").value.replaceAll("-", ".");

  if (gender.toLowerCase() === "male") {
    gender = "M";
  } else if (gender.toLowerCase() === "female") {
    gender = "F";
  }

  // Створюємо новий рядок для таблиці з отриманими даними
  var newRow = table.insertRow(-1); // Додаємо новий рядок в кінець таблиці
  var checkboxCell = newRow.insertCell(0);
  var groupCell = newRow.insertCell(1);
  var nameCell = newRow.insertCell(2);
  var genderCell = newRow.insertCell(3);
  var birthdayCell = newRow.insertCell(4);
  var statusCell = newRow.insertCell(5);
  var optionsCell = newRow.insertCell(6);

  var editButton = document.createElement("button");
  editButton.className = "button";
  editButton.id = "edit-row-button";
  optionsCell.appendChild(editButton);

  var space = document.createTextNode(" "); // Текстовий вузол з пробілом
  optionsCell.appendChild(space);

  var deleteButton = document.createElement("button");
  deleteButton.className = "button";
  deleteButton.id = "delete-row-button";
  optionsCell.appendChild(deleteButton);

  checkboxCell.innerHTML = '<div class="checkbox"></div>';
  groupCell.innerHTML = group;
  nameCell.innerHTML = firstName + " " + lastName;
  genderCell.innerHTML = gender;
  birthdayCell.innerHTML = birthday;
  statusCell.innerHTML = '<div class="circle green"></div>';
  addDeleteEventListeners();
  addCheckboxEventListeners();
});

// Функція для додавання обробників подій до кнопок видалення
function addDeleteEventListeners() {
  // Отримуємо всі кнопки "Delete" за їх id
  var deleteButtons = document.querySelectorAll("#delete-row-button");

  // Додаємо обробник подій для кожної кнопки "Delete"
  deleteButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      // Отримуємо ім'я користувача з відповідного рядка
      var userName = this.parentNode.parentNode.cells[2].textContent;

      // Відображаємо вікно підтвердження видалення
      var confirmation = confirm(
        "Are you sure want to delete user '" + userName + "'?"
      );

      // Якщо користувач підтвердив видалення
      if (confirmation) {
        // Видаляємо відповідний рядок з таблиці
        var row = this.parentNode.parentNode;
        row.parentNode.removeChild(row);
      }
    });
  });
}
