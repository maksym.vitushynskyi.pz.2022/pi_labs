document.addEventListener("DOMContentLoaded", function () {
  var addRowButton = document.getElementById("add-row-button");
  var modal = document.getElementById("add-row-modal");
  var closeModal = document.querySelector(".modal .close");
  var cancelButton = document.getElementById("cancel-button");

  addRowButton.addEventListener("click", function () {
    modal.style.display = "flex";
  });

  closeModal.addEventListener("click", function () {
    modal.style.display = "none";
  });

  cancelButton.addEventListener("click", function () {
    modal.style.display = "none";
  });

  var form = document.getElementById("add-record-form");

  form.addEventListener("submit", function (event) {
    event.preventDefault();

    var priority = document.getElementById("group").value;
    var taskName = document.getElementById("first-name").value;
    var about = document.getElementById("last-name").value;

    var container = document.querySelector(".container");

    var block = document.createElement("div");
    block.classList.add("block");

    if (priority === "Priority 1") {
      block.classList.add("red");
    } else if (priority === "Priority 2") {
      block.classList.add("yellow");
    } else if (priority === "Priority 3") {
      block.classList.add("green");
    }

    var head = document.createElement("div");
    head.classList.add("head");
    head.innerHTML = "<h2>" + taskName + "</h2>";

    var deleteButton = document.createElement("button");
    deleteButton.className = "button";
    deleteButton.id = "delete-row-button";
    deleteButton.addEventListener("click", function () {
      container.removeChild(block);
    });

    head.appendChild(deleteButton);
    block.appendChild(head);

    var content = document.createElement("div");
    content.classList.add("content");
    content.innerHTML = "<p>" + about + "</p>";
    block.appendChild(content);

    container.appendChild(block);

    modal.style.display = "none";
    form.reset();
  });
});
